create table annonces (
    id  serial,
    url  varchar(150),
    blacklist  Boolean,
    numero varchar(15),
    PRIMARY KEY(id)
);
create table account (
    pseudo  varchar(40),
    motdepasse  varchar(40),
    type_utilisateur varchar(15),
    age integer,
    sexe varchar(10),
    nom varchar(40),
    prenom varchar(40),
    adresse varchar(100),
    nom_entreprise varchar(40),
    secteur varchar(40),
    telephone varchar(15),
    PRIMARY KEY(pseudo)
);
create table liste (
  id serial,
  nom_liste varchar(60),
  id_annonce integer,
  username varchar(40),
  PRIMARY KEY(id),
  FOREIGN KEY(id_annonce) REFERENCES annonces(id),
  FOREIGN KEY(username) REFERENCES account(pseudo)
);
create table signalement (
  id_annonce integer,
  username  varchar(40),
  motif varchar(200),
  date varchar(15),
  PRIMARY KEY(id_annonce,username),
  FOREIGN KEY(id_annonce) REFERENCES annonces(id),
  FOREIGN KEY(username) REFERENCES account(pseudo)
);
create table historique (
  id serial,
  username  varchar(40),
  recherche integer,
  date varchar(15),
  PRIMARY KEY(id),
  FOREIGN KEY(recherche) REFERENCES annonces(id),
  FOREIGN KEY(username) REFERENCES account(pseudo)
);
