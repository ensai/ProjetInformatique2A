
# coding: utf-8
from dao.liste_d_envies import liste_d_envies_DAO as liste_dao



class ListeDEnvies:

    def __init__(self,nom,username):
        self.nom = nom
        self.username = username
        self.id_ann = liste_dao.lister(nom,username)


    def supprimer_annonce(self, id_ann):
        self.id_ann.remove(id_ann)
        liste_dao.delete(id_ann)

    def ajouter_annonce(self, id_ann):
        if not(id_ann in self.id_ann):
            self.id_ann.append(id_ann)
            liste_dao.create(self,id_ann)
        else:
            print("existe deja")

    def rechercher_annonce(self, id_ann):
        if id_ann in self.id_ann:
            return self.id_ann.index(id_ann)
        else:
            print("n'existe pas")

    def lister_liste(self):
        print (self.id_ann)
