from vue.menu import Menu
from Historique.Historique import Historique


if __name__ == '__main__':
    
    historique = Historique()
    current_vue = Menu(historique)

    while current_vue:
        current_vue.display_info()
        current_vue = current_vue.make_choice()
