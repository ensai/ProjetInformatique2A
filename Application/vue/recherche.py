from vue.abstract_vue import AbstractVue
from dao.filtre_de_recherche import Filtre_de_recherche
from PyInquirer import Separator, prompt
from vue.resultats import Resultats
from env import categories, localisation,proxies
from annonces.scrap import Scrap


class AffichageRecherche(AbstractVue):


    def make_choice(self):
        questions = [
                {
                    "type":"input",
                    "name":"recherche",
                    "message":"Que recherchez vous ?"
                },
                {
                    'type':'list',
                    'name':'categorie',
                    'message':"Dans quelle catégorie le recherchez vous ?",
                    'choices': categories.keys()
                },
                {
                    'type':'list',
                    'name':'region',
                    'message':"Dans quelle région ?",
                    'choices': localisation.keys()
                },
                {
                    'type': 'list',
                    'name': 'critere',
                    'message': 'Quel critere ?',
                    'choices': [
                        'Prix croissant',
                        Separator(),
                        'Prix décroissant',
                        Separator(),
                        'Plus Ancien',
                        Separator(),
                        'Plus Récent'
                    ]
                },
                {
                    'type':'list',
                    'name':'code postal',
                    'message':'Vous préférez ?',
                    'choices':[
                        'Chercher sans géolocalisation',
                        Separator(),
#                       'Chercher avec géolocalisation automatique',
#                       Separator(),
                        'Saisir un code postal'
                    ]
                }
            ]

        if self.user and (self.user.type_d_utilisateur() == "Administrateur" or self.user.type_d_utilisateur() == "Professionnel"):
            questions.append({"type":"list","name":"pro","message":"Souhaitez-vous faire des recherches uniquement sur des professionnels ?","choices":["Oui",Separator(),"Non"]})

        filtre = Filtre_de_recherche()

        reponses = prompt(questions)

        filtre.definir_recherche_pro("pro" in reponses.keys() and reponses["pro"] == "Oui")
        filtre.definir_recherche(reponses["recherche"])
        filtre.definir_categorie(categories[reponses["categorie"]])
        filtre.definir_localisation(localisation[reponses["region"]])

        if reponses["code postal"] == 'Saisir un code postal':
            reponse_code_postal = input("Votre code postal ?")

        elif reponses["code postal"] == 'Chercher avec géolocalisation automatique':
            scrap = Scrap('https://www.coordonnees-gps.fr/ma-position', proxies)
            reponse_code_postal = scrap.get_my_position()
        else:
            reponse_code_postal = None

        #Vérifier que le code postal est valide, il y a bien 5 chiffres...
        filtre.definir_code_postal(reponse_code_postal)


        if reponses["critere"] == "Prix croissant":
            filtre.definir_critere("price")
            filtre.definir_ordre("asc")

        elif reponses["critere"] == "Prix décroissant":
            filtre.definir_critere("price")
            filtre.definir_ordre("desc")

        elif reponses["critere"] == "Plus Ancien":
            filtre.definir_critere("time")
            filtre.definir_ordre("asc")

        elif reponses["critere"] == "Plus Récent":
            filtre.definir_critere("time")
            filtre.definir_ordre("desc")

        resultats = Resultats(self.historique,self.user)
        resultats.set_recherche(filtre)
        return resultats
