from vue.abstract_vue import AbstractVue

from dao.compte_dao import CompteDao

from PyInquirer import prompt, Validator, ValidationError, Separator


class PasswordValidator(Validator):
    def validate(self, document):
        ok = len(document.text) > 5
        if not ok:
            raise ValidationError(
                message='Votre mot de passe doit faire au moins 6 caractères',
                cursor_position=len(document.text))  # Move cursor to end


questions = [
     {
        'type': 'list',
        'name': "Type d'utilisateur",
        'message': 'quel genre d"ulisateur êtes-vous ?',
        'choices': [
            'Particulier', 'Professionnel','Administrateur',
            Separator(),
            'Quitter',
        ]
    },
    {
        'type': 'input',
        'name': 'pseudonyme',
        'message': 'Quel est votre pseudonyme ?',

    },
    {
        'type': 'password',
        'name': 'mot de passe',
        'message': 'Quel  votre mot de passe ?',
        'validate': PasswordValidator
    }
]


class CreationCompte(AbstractVue):


    def make_choice(self):
        answers = prompt(questions)
        if answers["Type d'utilisateur"]=="Quitter":
            from vue.menu import Menu
            return Menu(self.historique)

        elif CompteDao.pseudo_disponible(answers['pseudonyme']):
            CompteDao.create( answers['pseudonyme'], answers['mot de passe'],answers["Type d'utilisateur"])
            print( "compte créé avec succès")

            menu = Menu(self.historique)
            return menu
        else:
            print("{} est déjà utilisé, merci d'en choisir un autre ;) ".format(
                answers['pseudonyme']))
            print("erreur dans le pseudonyme/le mot de passe")
            reponse=input("Voulez-vous réessayer?\n[O]/N ")
            if reponse=="N":
                from vue.menu import Menu
                menu = Menu(self.historique,compte)
                return menu
            else:
                return self.make_choice()
