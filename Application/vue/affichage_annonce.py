from vue.abstract_vue import AbstractVue
from PyInquirer import Separator, prompt
from dao.liste_d_envies import Liste_d_envies_DAO


class AffichageAnnonce(AbstractVue):

    def __init__(self,annonce,page_precedente,historique,user=None):
        self.user = user
        self.historique = historique
        self.annonce = annonce
        self.page_precedente = page_precedente
        if annonce.url:
            self.historique.nouvelle_recherche(self.annonce.id)
            self.questions = [
                {
                    'type': 'list',
                    'name': 'retour',
                    'message': 'Menu ?',
                    'choices': [
                        "Copier le lien dans le presse-papier",
                        Separator(),
                        "Exporter l'annonce",
                        Separator(),
                        'Retour aux annonces',
                        Separator(),
                        'Retour au menu',
                    ]
                }
            ]

            if self.user:
                self.questions[0]['choices'] = ["Ajouter à une liste d'envie",Separator(),
                "Signaler l'annonce",Separator()]+self.questions[0]['choices']
            if self.annonce.numero == "Numéro masqué":
                self.questions[0]['choices'] = ['Obtenir le numéro de téléphone',Separator()]+self.questions[0]['choices']


    def display_info(self):
        print(self.annonce)

    def make_choice(self):
        if self.annonce.url is None:
            print(self.annonce.titre)
            return self.page_precedente
        reponse = prompt(self.questions)
        if reponse["retour"] == "Retour au menu":
            from vue.menu import Menu
            menu = Menu(self.historique,self.user)
            return menu
        elif reponse["retour"] == "Retour aux annonces":
            return self.page_precedente
        elif reponse["retour"] == "Copier le lien dans le presse-papier":
            import pyperclip
            pyperclip.copy(self.annonce.url)
            return self
        elif reponse["retour"] == "Obtenir le numéro de téléphone":
            self.annonce.get_numero()
            self.questions[0]['choices'] = self.questions[0]['choices'][2:]
            return self
        elif reponse["retour"] == "Signaler l'annonce":
            from vue.signalement_annonce import SignalementAnnonce
            return SignalementAnnonce(self)
        elif reponse["retour"] == "Exporter l'annonce":
            from import_export.export import Export
            path=input("Dans que dossier voulez-vous enregistrer l'exportation?\n")
            if not path[-1]=="/":
                path+="/"
            path+=input("Sous quel nom?")
            path+=".json"
            Export.exporter_annonce(self.annonce,path)
            print("Exportation terminée")
            return self
        elif reponse["retour"] == "Ajouter à une liste d'envie":
            questions = [
                {
                    'type': 'list',
                    'name': "liste d'envies",
                    'message': 'Ensemble des envies',
                    'choices': []
                }
            ]
            liste_des_listes_d_envies = Liste_d_envies_DAO.ensemble_listes(self.user.username)
            for x in liste_des_listes_d_envies :
                questions[0]["choices"].append(x[0])
                questions[0]["choices"].append(Separator())
            questions[0]["choices"].append("Créer une liste d'envie")
            questions[0]["choices"].append(Separator())
            questions[0]["choices"].append("Retour")
            answer = prompt(questions)
            if answer["liste d'envies"] == "Retour":
                return self
            elif answer["liste d'envies"] == "Créer une liste d'envie":
                nom = input("Nom de la nouvelle liste ?\n")
                Liste_d_envies_DAO.create(nom,self.annonce.id,self.user.username)
                print("annonce ajoutée")
                return self
            else:
                for x in liste_des_listes_d_envies:
                    if answer["liste d'envies"] == x[0]:
                        Liste_d_envies_DAO.create(x[0],self.annonce.id,self.user.username)
                        print("annonce ajoutée")
                        return self
        else:
            print("erreur !!!")
