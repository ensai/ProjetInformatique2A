from vue.abstract_vue import AbstractVue
from PyInquirer import Separator, prompt


class GererCompte(AbstractVue):

    def display_info(self):
        print(self.user)
        

    def make_choice(self):
        questions =[
            {
                'type': 'list',
                'name': 'gestion',
                'message': 'Choix ?',
                'choices': [
                    'Changer son mot de passe',
                    Separator(),
                    "Changer son genre",
                    Separator(),
                    "Changer son nom",
                    Separator(),
                    "Changer son prenom",
                    Separator(),
                    "Changer son adresse",
                    Separator(),
                    "Changer son age",
                ]
            }
        ]

        if self.user.type_d_utilisateur() == "Administrateur" or self.user.type_d_utilisateur() == "Professionnel":
            questions[0]["choices"].append(Separator())
            questions[0]["choices"].append("Changer son telephone")
            questions[0]["choices"].append(Separator())
            questions[0]["choices"].append("Changer son secteur d'activité")
            questions[0]["choices"].append(Separator())
            questions[0]["choices"].append("Changer le nom de son entreprise")

        questions[0]["choices"].append(Separator())
        questions[0]["choices"].append("Supprimer son compte")
        questions[0]["choices"].append(Separator())
        questions[0]["choices"].append('Retour au menu')


        answer = prompt(questions)

        if answer["gestion"] == "Supprimer son compte":
            reponse = input("Voulez-vous vraiment supprimer votre compte?\nO/[N]")
            if reponse == "O":
                self.user.supprimer_compte()
                from vue.menu import Menu
                return Menu(self.historique)
            else:
                return self
        elif answer["gestion"] == "Changer son mot de passe":
            password= input("Quel motdepasse?\n")

            self.user.change_password(password)
            return self
        elif answer["gestion"] == "Changer son genre":
            question = [
                {
                    'type': 'list',
                    'name': 'sexe',
                    'message': 'Genre ?',
                    'choices': [
                        'Masculin',
                        Separator(),
                        "Féminin",
                        Separator(),
                        "Annuler",
                    ]
                }
            ]
            reponse = prompt(question)
            if not reponse["sexe"] == "Annuler":
                self.user.change_sexe(reponse["sexe"])
            return self
        elif answer["gestion"] == "Changer son nom":
            nom = input("Quel nom ?\n")
            self.user.change_nom(nom)
            return self
        elif answer["gestion"] == "Changer son prenom":
            prenom = input("Quel prénom ?\n")
            self.user.change_prenom(prenom)
            return self
        elif answer["gestion"] == "Changer son adresse":
            adresse = input("Quel adresse ?\n")
            self.user.change_adresse(adresse)
            return self
        elif answer["gestion"] == "Changer son age":
            age = input("Quel âge ?\n")
            self.user.change_age(age)
            return self

        elif answer["gestion"]== "Changer son telephone":
            telephone= input("Quel numéro ?\n")
            self.user.change_phone(telephone)
            return self
        elif answer["gestion"]== "Changer son secteur d'activité":
            secteur  = input("Quel activité ?\n")
            self.user.change_secteur(secteur)
            return self
        elif answer["gestion"]== "Changer le nom de son entreprise":
            nom_entreprise = input("Quel nom pour entreprise ?\n")
            self.user.change_nom_entreprise(nom_entreprise)
            return self





        elif answer["gestion"] == "Retour au menu":
            from vue.menu import Menu
            return Menu(self.historique,self.user)
        else:
            print("erreur !!!")
