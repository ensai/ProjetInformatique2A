from vue.abstract_vue import AbstractVue
from PyInquirer import Separator, prompt
from dao.compte_dao import CompteDao

class GererUtilisateurs(AbstractVue):

    def __init__(self,historique,user):
        self.user = user
        self.historique = historique
        self.liste_users = CompteDao.list_pseudo()
        self.current_user = None

    def make_choice(self):
        if self.current_user is None:
            questions = [
                {
                    'type': 'list',
                    'name': 'gestion',
                    'message': 'Choisir un utilisateur ?',
                    'choices': []
                }
            ]

            for x in self.liste_users:
                questions[0]["choices"].append("Utilisateur : "+str(x[0])+"\nType : "+str(x[1]))
                questions[0]["choices"].append(Separator())
            questions[0]["choices"].append("Retour au menu")
        else:
            questions = [
                {
                    'type': 'list',
                    'name': 'gestion',
                    'message': "Gestion de l'utilisateur "+self.current_user,
                    'choices': ["Prendre possession de l'utilisateur",Separator(),"Supprimer l'utilisateur",Separator(),"Retour"]
                }
            ]

        reponse = prompt(questions)
        if reponse["gestion"] == "Retour":
            self.current_user = None
            return self
        elif reponse["gestion"] == "Retour au menu":
            from vue.menu import Menu
            return Menu(self.historique,self.user)
        elif reponse["gestion"] == "Supprimer l'utilisateur":
            from dao.compte_dao import CompteDao
            CompteDao.get(self.current_user).supprimer_compte()
            return GererUtilisateurs(self.historique,self.user)
        elif reponse["gestion"] == "Prendre possession de l'utilisateur":
            from dao.compte_dao import CompteDao
            from dao.historique import HistoriqueDAO
            HistoriqueDAO.get(self.historique,self.current_user)
            return Menu(self.historique,CompteDao.get(self.current_user))
        else:
            for x in self.liste_users:
                if reponse["gestion"] == "Utilisateur : "+str(x[0])+"\nType : "+str(x[1]):
                    self.current_user=x[0]
                    return self
