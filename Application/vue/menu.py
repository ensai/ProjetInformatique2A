from vue.abstract_vue import AbstractVue
from PyInquirer import Separator, prompt



class Menu(AbstractVue):

    def make_choice(self):
        questions = [
            {
                'type': 'list',
                'name': 'authentification',
                'message': 'Menu ?',
                'choices': [
                    'Recherche annonces',
                    Separator(),
                    "Voir son historique",
                    Separator(),
                    'Quitter',
                ]
            }
        ]

        if not(self.user):
            questions[0]['choices'] = ['Se connecter', Separator(), 'Créer un compte', Separator(),] + questions[0]['choices']
        else:
            if self.user.type_d_utilisateur() == "Administrateur":
                questions[0]['choices'] = ['Gérer son compte', Separator(),'Gérer les utilisateurs', Separator(),'Gérer les annonces', Separator(), "Voir ses listes d'envies", Separator(),"Se déconnecter",Separator()] + questions[0]['choices']
            else:
                questions[0]['choices'] = ['Gérer son compte', Separator(), "Voir ses listes d'envies", Separator(),"Se déconnecter",Separator()] + questions[0]['choices']

        reponse = prompt(questions)
        if reponse['authentification'] == 'Se connecter':
            from vue.compte_authentification import Authentification
            return Authentification(self.historique,self.user)
        elif reponse['authentification'] == 'Créer un compte':
            from vue.creer_un_compte import CreationCompte
            return CreationCompte(self.historique,self.user)
        elif reponse['authentification'] == 'Recherche annonces':
            from vue.recherche import AffichageRecherche
            return AffichageRecherche(self.historique,self.user)
        elif reponse['authentification'] == 'Se déconnecter':
            from Historique.Historique import Historique
            return Menu(Historique())
        elif reponse['authentification'] == 'Quitter':
            print("Au revoir")
        elif reponse['authentification'] == 'Gérer son compte':
            from vue.gerer_compte import GererCompte
            return GererCompte(self.historique,self.user)
        elif reponse['authentification'] == 'Gérer les utilisateurs':
            from vue.gerer_utilisateurs import GererUtilisateurs
            return GererUtilisateurs(self.historique,self.user)
        elif reponse['authentification'] == 'Gérer les annonces':
            from vue.gerer_annonces import GererAnnonces
            return GererAnnonces(self.historique,self.user)
        elif reponse['authentification'] == "Voir son historique":
            from vue.historique import AffichageHistorique
            return AffichageHistorique(self.historique,self.user)
        elif reponse['authentification'] == "Voir ses listes d'envies":
            from vue.liste_d_envies import AffichageEnvies
            return AffichageEnvies(self.historique,self.user)
        else:
            print("A venir !!!!")
            return self
