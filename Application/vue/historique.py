from vue.abstract_vue import AbstractVue
from PyInquirer import Separator, prompt
from dao.annonce import AnnonceDao
from vue.affichage_annonce import AffichageAnnonce
import psycopg2

MY_ID ="id1087"
connection = psycopg2.connect(host="sgbd-eleves.domensai.ecole", port="5432",database=MY_ID, user=MY_ID, password=MY_ID)

class AffichageHistorique(AbstractVue):

    def __init__(self,historique,user):
        self.historique = historique
        self.user = user
        self.question =  [
            {
                'type': 'list',
                'name': 'historique',
                'message': 'Historique',
                'choices': []
            }
        ]

        for couple in self.historique.liste_couple_id_date:
            self.question[0]["choices"].append(AnnonceDao.get(couple[0]).titre+"\n Date de consultation : "+str(couple[1]))
            self.question[0]["choices"].append(Separator())
        self.question[0]["choices"].append("Exporter l'historique")
        self.question[0]["choices"].append(Separator())
        self.question[0]["choices"].append("Vider l'historique")
        self.question[0]["choices"].append(Separator())
        self.question[0]["choices"].append("Supprimer une annonce de l'historique")
        self.question[0]["choices"].append(Separator())
        self.question[0]["choices"].append("Retour au menu")




    def make_choice(self):

        reponse = prompt(self.question)
        if reponse["historique"] == 'Retour au menu':
            from vue.menu import Menu
            return Menu(self.historique,self.user)

        elif reponse["historique"] == "Exporter l'historique":
            from import_export.export import Export
            from liste_d_envies.liste_d_envies import ListeDEnvies
            path=input("Dans que dossier voulez-vous enregistrer l'exportation?\n")
            if not path[-1]=="/":
                path+="/"
            path+=input("Sous quel nom?")
            path+=".json"
            Export.exporter_historique(self.historique,path)
            print("Exportation terminée")
            return self

        elif reponse["historique"] == "Vider l'historique":
            from dao.historique import HistoriqueDAO
            confirmation = input("Voulez-vous vraiment supprimer la liste d'envie?\nO/[N]  ")
            if confirmation=="O":
                HistoriqueDAO.drop(self.user.username)
            HistoriqueDAO.get(self.historique,self.user.username)
            return AffichageHistorique(self.historique,self.user)

        elif reponse["historique"] == "Supprimer une annonce de l'historique":
            from dao.historique import HistoriqueDAO
            question_suppression = [
                {
                    'type': "list",
                    'name': "suppression",
                    'message': 'Quel annonce doît être supprimée ?',
                    'choices': [],
                }
            ]
            for couple in self.historique.liste_couple_id_date:
                question_suppression[0]["choices"].append(AnnonceDao.get(couple[0]).titre+"\n Date de consultation : "+str(couple[1]))
                question_suppression[0]["choices"].append(Separator())
            question_suppression[0]["choices"].append("Retour")
            reponse_suppression = prompt(question_suppression)
            for couple in self.historique.liste_couple_id_date:
                annonce = AnnonceDao.get(couple[0])
                if reponse_suppression["suppression"] == annonce.titre+"\n Date de consultation : "+str(couple[1]):
                    HistoriqueDAO.delete(couple,self.user.username)
                    HistoriqueDAO.get(self.historique,self.user.username)
            return AffichageHistorique(self.historique,self.user)

        else:
            for couple in self.historique.liste_couple_id_date:
                annonce = AnnonceDao.get(couple[0])
                if reponse["historique"] == annonce.titre+"\n Date de consultation : "+str(couple[1]):


                    return AffichageAnnonce(annonce,self,self.historique,self.user)
