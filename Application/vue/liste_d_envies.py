from vue.abstract_vue import AbstractVue
from PyInquirer import Separator, prompt
from dao.liste_d_envies import Liste_d_envies_DAO
from dao.annonce import AnnonceDao
from vue.affichage_annonce import AffichageAnnonce


class AffichageEnvies(AbstractVue):

    def __init__(self,historique,user):
        self.user = user
        self.historique = historique
        self.listes = Liste_d_envies_DAO.ensemble_listes(user.username)
        self.current_list = list()

    def make_choice(self):
        question = [
            {
                'type': "list",
                'name': "liste_d_envies",
                'message': 'Ensemble des envies',
                'choices': [],
            }
        ]

        if not self.current_list:
            for x in self.listes:
                question[0]["choices"].append(x[0])
                question[0]["choices"].append(Separator())
            question[0]["choices"].append("Importer une liste d'envies")
            question[0]["choices"].append(Separator())
            question[0]["choices"].append("Retour au menu")
        else:
            for x in self.current_list:
                question[0]["choices"].append(x.titre)
                question[0]["choices"].append(Separator())
            question[0]["choices"].append("Exporter la liste d'envie")
            question[0]["choices"].append(Separator())
            question[0]["choices"].append("Supprimer la liste d'envie")
            question[0]["choices"].append(Separator())
            question[0]["choices"].append("Supprimer un élément de la liste d'envie")
            question[0]["choices"].append(Separator())
            question[0]["choices"].append("Retour aux listes d'envies")

        reponse = prompt(question)
        if reponse["liste_d_envies"] == 'Retour au menu':
            from vue.menu import Menu
            return Menu(self.historique,self.user)

        elif reponse["liste_d_envies"] == "Retour aux listes d'envies":
            self.current_list = list()
            return self.make_choice()

        elif reponse["liste_d_envies"] == "Supprimer un élément de la liste d'envie":
            question_suppression = [
                {
                    'type': "list",
                    'name': "suppression",
                    'message': 'Quel annonce doît être supprimée ?',
                    'choices': [],
                }
            ]
            for x in self.current_list:
                question_suppression[0]["choices"].append(x.titre)
                question_suppression[0]["choices"].append(Separator())
            question_suppression[0]["choices"].append("Retour")
            reponse_suppression = prompt(question_suppression)
            for annonce in self.current_list:
                if reponse_suppression["suppression"] == annonce.titre:
                    Liste_d_envies_DAO.delete(annonce.id,self.user.username,self.current_list_name)
                    self.current_list.remove(annonce)
            return self.make_choice()

        elif reponse["liste_d_envies"] == "Supprimer la liste d'envie":
            confirmation = input("Voulez-vous vraiment supprimer la liste d'envie?\nO/[N]  ")
            if confirmation=="O":
                Liste_d_envies_DAO.delete_liste(self.user.username,self.current_list_name)
            return AffichageEnvies(self.historique,self.user)

        elif reponse["liste_d_envies"] == "Importer une liste d'envies":
            from import_export.importer import Importer
            path=input("Dans quel dossier est située le fichier à importer?\n")
            if not path[-1]=="/":
                path+="/"
            path+=input("Sous quel nom?\n")
            path+=".json"
            Importer.importer_liste(path)
            print("Importation terminée")
            return AffichageEnvies(self.historique,self.user)

        elif reponse["liste_d_envies"] == "Exporter la liste d'envie":
            from import_export.export import Export
            from liste_d_envies.liste_d_envies import ListeDEnvies
            path=input("Dans quel dossier voulez-vous enregistrer l'exportation?\n")
            if not path[-1]=="/":
                path+="/"
            path+=input("Sous quel nom?\n")
            path+=".json"
            Export.exporter_list(ListeDEnvies(self.current_list_name,self.user.username),path)
            print("Exportation terminée")
            return self

        elif self.current_list:
            for annonce in self.current_list:
                if reponse["liste_d_envies"] == annonce.titre:
                    return AffichageAnnonce(annonce,self,self.historique,self.user)

        else:
            for x in self.listes:
                if reponse["liste_d_envies"] == x[0]:
                    raw_list = Liste_d_envies_DAO.lister(x[0],self.user.username)
                    self.current_list_name = x[0]
                    for id_annonce in raw_list:
                        self.current_list.append(AnnonceDao.get(id_annonce[0]))
                    return self
