from vue.abstract_vue import AbstractVue
from PyInquirer import prompt

questions =[
    {
        'type': 'input',
        'name': 'motif',
        'message': 'Pour quel motif ?'
    },
    {
        'type': 'input',
        'name': 'confirmation',
        'message': 'Voulez-vous vraiment signaler cette annonce ? O/[N]'
    }
]

class SignalementAnnonce(AbstractVue):

    def __init__(self,page_precedente):
        self.user = page_precedente.user
        self.historique = page_precedente.historique
        self.page_precedente = page_precedente

    def make_choice(self):
        reponse = prompt(questions)
        if reponse["confirmation"] == "O":
            from dao.annonces_signalees import SignalementAnnonceDAO
            import time
            SignalementAnnonceDAO.create(self.page_precedente.annonce.id,self.page_precedente.user.username,reponse["motif"],time.strftime('%y/%m/%d %H:%M',time.localtime()))
            print("Annonce signalée")
            return self.page_precedente
        else:
            return self.page_precedente
