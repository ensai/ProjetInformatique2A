from vue.abstract_vue import AbstractVue
from PyInquirer import Separator, prompt
from dao.annonces_signalees import SignalementAnnonceDAO
from dao.annonce import AnnonceDao

class GererAnnonces(AbstractVue):

    def __init__(self,historique,user):
        self.user = user
        self.historique = historique
        self.liste_annonces_signalees = SignalementAnnonceDAO.get()
        self.current_annonce = None

    def make_choice(self):
        if self.current_annonce is None:
            questions = [
                {
                    'type': 'list',
                    'name': 'gestion',
                    'message': 'Annonces signalées ?',
                    'choices': []
                }
            ]

            for x in self.liste_annonces_signalees:
                annonce = AnnonceDao.get(x[0])
                questions[0]["choices"].append("Titre : "+annonce.titre+"\nSignalée par : "+str(x[1])+"\nDate : "+str(x[3]))
                questions[0]["choices"].append(Separator())
            questions[0]["choices"].append("Blacklister/Déblacklister une annonce grâce à son id")
            questions[0]["choices"].append(Separator())
            questions[0]["choices"].append("Retour au menu")
        else:
            questions = [
                {
                    'type': 'list',
                    'name': 'gestion',
                    'message': "Gestion de l'annonce : "+ self.current_annonce.titre,
                    'choices': ["Regarder l'annonce",Separator(),"Blacklister l'annonce",Separator(),"Supprimer le signalement",Separator(),"Retour"]
                }
            ]

        reponse = prompt(questions)
        if reponse["gestion"] == "Retour":
            self.current_annonce = None
            return self
        elif reponse["gestion"] == "Retour au menu":
            from vue.menu import Menu
            return Menu(self.historique,self.user)
        elif reponse["gestion"] == "Regarder l'annonce":
            from vue.affichage_annonce import AffichageAnnonce
            return AffichageAnnonce(self.current_annonce,self,self.historique,self.user)
        elif reponse["gestion"] == "Supprimer le signalement":
            SignalementAnnonceDAO.delete(self.current_annonce.id,self.current_user)
            return GererAnnonces(self.historique,self.user)
        elif reponse["gestion"] == "Blacklister l'annonce":
            if self.current_annonce.url:
                AnnonceDao.blacklist(self.current_annonce.id)
                SignalementAnnonceDAO.delete(self.current_annonce.id,self.current_user)
                return GererAnnonces(self.historique,self.user)
            else:
                print("l'annonce est déjà blacklistée")
                return self
        elif reponse["gestion"] == "Blacklister/Déblacklister une annonce grâce à son id":
            id = input("Quel est l'identifiant de l'annonce en question?\n")
            AnnonceDao.blacklist(id)
            return GererAnnonces(self.historique,self.user)
        else:
            for x in self.liste_annonces_signalees:
                annonce = AnnonceDao.get(x[0])
                if "Titre : "+annonce.titre+"\nSignalée par : "+str(x[1])+"\nDate : "+str(x[3]):
                    self.current_annonce=annonce
                    self.current_user = x[1]
                    return self
