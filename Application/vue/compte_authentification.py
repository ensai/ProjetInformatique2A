from vue.abstract_vue import AbstractVue
from PyInquirer import prompt
from dao.compte_dao import CompteDao

questions = [
    {
        'type': 'input',
        'name': 'pseudonyme',
        'message': 'Quel est votre pseudonyme ?',

    },
    {
        'type': 'password',
        'name': 'mot de passe',
        'message': 'Quel est votre mot de passe ?'
    }
]




class Authentification(AbstractVue):

    def make_choice(self):

        answers = prompt(questions)

        compte = CompteDao.get(answers["pseudonyme"])

        if compte and compte.password == answers["mot de passe"]:
            print("connexion réussie")
            self.historique.connection(answers["pseudonyme"])
            from vue.menu import Menu
            menu = Menu(self.historique,compte)
            return menu
        else:
            print("erreur dans le pseudonyme/le mot de passe")
            reponse=input("Voulez-vous réessayer?\n[O]/N ")
            if reponse=="N":
                from vue.menu import Menu
                menu = Menu(self.historique,compte)
                return menu
            else:
                return self.make_choice()
