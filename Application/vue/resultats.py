from unidecode import unidecode
from vue.abstract_vue import AbstractVue
from annonces.annonce import Annonce
from annonces.recherche import Recherche
from vue.affichage_annonce import AffichageAnnonce
from PyInquirer import Separator, prompt


def questions(recherche):
    resu = list()
    for x in recherche[:min(10,len(recherche))]:
        resu += [unidecode(str(x[0].text))+"\nPrix : "+x[1]+"\nDate de parution : "+x[2]] +  [Separator()]
    resu += ["Retour au menu"]
    return([
        {
            'type': 'list',
            'name': 'resultats',
            'message': 'Recherche ?',
            'choices': resu
        }
    ])

class Resultats(AbstractVue):

    def set_recherche(self,filtre):
        recherche = Recherche(filtre)
        self.recherche = recherche.resultats

    def make_choice(self):
        reponse = prompt(questions(self.recherche))
        if reponse["resultats"] == "Retour au menu":
            from vue.menu import Menu
            return Menu(self.historique,self.user)

        else:
            for x in self.recherche:
                if reponse["resultats"] == unidecode(str(x[0].text))+"\nPrix : "+x[1]+"\nDate de parution : "+x[2]:
                    if x[3]=="leboncoin":
                        annonce = Annonce("https://www.leboncoin.fr"+x[0].getparent().getparent().getparent().get("href"))
                    elif x[3]=="Vivastreet":
                        annonce = Annonce(x[0].getparent().getparent().getparent().get("href"))
                    return AffichageAnnonce(annonce,self,self.historique,self.user)
