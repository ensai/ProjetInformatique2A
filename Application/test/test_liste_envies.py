# -*- coding: utf-8 -*-
"""
Created on Fri Oct 12 11:04:47 2018

@author: id1222
"""

from liste_d_envies.liste_d_envies import ListeDEnvies as liste
from dao.liste_d_envies import Liste_d_envies_DAO as liste_dao



def test_ajouter_liste():
    l=liste("l")
    l.ajouter_annonce('id_ann')
    assert 'id_ann'in l.id_ann
    assert 'id_ann' in liste_dao.lister(l)

def test_supprimer_annonce():
    l=liste("l")
    l2 = liste("l2")
    l.ajouter_annonce('id_ann')
    l2.ajouter_annonce('id_ann')
    l.supprimer_annonce('id_ann')
    assert not('id_ann' in l.id_ann)
    assert liste_dao.lister(l) is None or not ('id_ann' in liste_dao.lister(l))
    assert 'id_ann' in l2.id_ann
    assert 'id_ann' in liste_dao.lister(l2)

def test_rechercher_liste():
    l=liste("l")
    l.ajouter_annonce('id_ann')
    index=l.rechercher_annonce('id_ann')
    assert l.id_ann[index] == 'id_ann'
