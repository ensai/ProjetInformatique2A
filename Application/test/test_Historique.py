# -*- coding: utf-8 -*-
"""
Created on Fri Oct 12 14:31:34 2018

@author: id1182
"""
import psycopg2
import time
import Historique.py



MY_ID ="id1087"
connection = psycopg2.connect(host="sgbd-eleves.domensai.ecole", port="5432",database=MY_ID, user=MY_ID, password=MY_ID)


def test_init():
    supprimer_tout('bonjour')
    a=Historique('bonjour')
    a.nouvelle_recherche(8),temps1=time.strftime('%d/%m/%y %H:%M',time.localtime())
    a=Historique('bonjour')
    assert a.historique==[(8,temps1)]
    supprimer_tout('bonjour')

def test_nouvelle_recherche():
    supprimer_tout('salut')
    supprimer_tout('bonjour')
    a=Historique('salut')
    b=Historique('bonjour')
    a.nouvelle_recherche(7),temps1=time.strftime('%d/%m/%y %H:%M',time.localtime())
    b.nouvelle_recherche(8)
    assert a.Historique==[(7,temps1)]
    assert b.historique==[(8,temps1)]
    a.nouvelle_recherche(8),temps2=time.strftime('%d/%m/%y %H:%M',time.localtime())
    assert a.Historique==[(8,temps2),(7,temps1)]
    
    supprimer_tout('salut')
    supprimer_tout('bonjour')
    
    
def test_supprimer_recherche():
    supprimer_tout('salut')
    a=Historique('salut')
    a.nouvelle_recherche(7)
    a.nouvelle_recherche(8),temps1=time.strftime('%d/%m/%y %H:%M',time.localtime())
    a.supprimer_recherche(7)
    assert a.historique==[(8,temps1)]
    cur=connection.cursor()
    cur.execute("Select * from historique Where username = 'salut';")
    rows= cur.fetchall()
    cur.close()
    assert [(rows[0][3],rows[0][4])]==a.historique
       
    supprimer_tout('salut')
    a=Historique('salut')
    a.nouvelle_recherche(7),temps2=time.strftime('%d/%m/%y %H:%M',time.localtime())
    a.nouvelle_recherche(8)
    a.supprimer_recherche(8)
    assert a.historique==[(7,temps2)]
    cur=connection.cursor()
    cur.execute("Select * from historique Where username = 'salut';")
    rows= cur.fetchall()
    cur.close()
    assert [(rows[0][3],rows[0][4])]==a.historique
    supprimer_tout('salut')
    
            
            
            
            
    