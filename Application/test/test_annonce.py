from annonces.annonce import Annonce
from dao.annonce import AnnonceDao
import time

def test_init():
    #On teste sur une annonce quelconque de leboncoin
    annonce = Annonce("https://www.leboncoin.fr/jeux_jouets/1526084908.htm/")
    annonce.get_numero()
    assert annonce.url == "https://www.leboncoin.fr/jeux_jouets/1526084908.htm/"
    assert annonce.titre == "Warhammer 40k chaos"
    assert annonce.prix == "10 EUR"
    assert annonce.localisation == 'Lanester  56600'
    assert annonce.vendeur == 'kiki77'
    assert annonce.date == '18/11/2018 a 23h57'
    assert annonce.numero == '0613704600'
    assert annonce.description == "Vends pieces en metal du dreadnought du chaos pour Warhammer 40k. \nPieces neuves jamais peintes.\nContact par tel ou sms prefere pour reponse rapide."
    #On teste sur une annonce quelconque de vivastreet
    annonce = Annonce("http://www.vivastreet.com/livres-occasion/la-valentine-13011/warhammer-roi-des-tombes-/190410474")
    assert annonce.url == "http://www.vivastreet.com/livres-occasion/la-valentine-13011/warhammer-roi-des-tombes-/190410474"
    assert annonce.titre == "Livre occasion Warhammer Roi des tombes  Marseille - 13011"
    assert annonce.prix == " 15EUR "
    assert annonce.localisation == 'Marseille - 13011'
    assert annonce.vendeur == 'Vivastreet_7982007451'
    assert annonce.date == ' le 05/09/2018 '
    assert annonce.numero == '06 24 76 83 59 '
    assert annonce.description == "Warhammer Roi des tombes \nBon etat \nA retirer sur place Marseille 11 "

def test_str():
    #On teste sur une annonce quelconque
    resu = "Titre : Warhammer 40k chaos\nPrix : 10 EUR\nDescription : Vends pieces en metal du dreadnought du chaos pour Warhammer 40k. \nPieces neuves jamais peintes.\nContact par tel ou sms prefere pour reponse rapide.\nLocalisation : Lanester  56600\nVendeur : kiki77\nContact : 0613704600\nDate de mise en ligne : 18/11/2018 a 23h57"
    annonce = Annonce("https://www.leboncoin.fr/jeux_jouets/1526084908.htm/")
    annonce.get_numero()
    assert resu == str(annonce)

def test_dao_create_select():
    #l'init fait appel à AnnonceDao.create()
    ancienne_annonce = Annonce("https://www.leboncoin.fr/jeux_jouets/1526084908.htm/")
    ancienne_annonce.get_numero()
    #vérifie les annonces
    nouvelle_annonce = AnnonceDao.get(ancienne_annonce.id)
    nouvelle_annonce.get_numero()
    assert ancienne_annonce.id == nouvelle_annonce.id
    assert ancienne_annonce.titre == nouvelle_annonce.titre
    assert ancienne_annonce.url == nouvelle_annonce.url
    assert ancienne_annonce.prix == nouvelle_annonce.prix
    assert ancienne_annonce.description == nouvelle_annonce.description
    #on vérifie que les indices sans associations ne rendent rien
    annonce_inexistante = AnnonceDao.get('4242')
    assert annonce_inexistante.url is None

def test_blacklist():
    annonce = Annonce("https://www.leboncoin.fr/jeux_jouets/1526084908.htm/")
    annonce.get_numero()
    #On teste que l'annonce n'est plus accessible après un blacklist
    AnnonceDao.blacklist(annonce.id)
    annonce_refusee = AnnonceDao.get(annonce.id)
    assert annonce_refusee.url is None
    #On teste que l'annonce est de nouveau accessible après un deuxième blacklist (reversibilité)
    AnnonceDao.blacklist(annonce.id)
    annonce_aceptee = AnnonceDao.get(annonce.id)
    assert annonce_aceptee == annonce
