from dao.compte_dao import CompteDao as compte_dao

def test_creation_et_get():
    compte_dao.create('TestPseudo', 'MotDePassePseudo')
    assert compte_dao.get('TestPseudo')
    compte_dao.delete(compte_dao.get('TestPseudo'))

def test_delete():
    compte_dao.create('TestPseudo', 'MotDePassePseudo')
    assert compte_dao.get('TestPseudo')
    compte_dao.delete(compte_dao.get('TestPseudo'))
    assert not compte_dao.get('TestPseudo')

def test_valid_pseudo_connu():
    compte_dao.create('TestPseudo', 'MotDePassePseudo')
    assert not compte_dao.pseudo_disponible('TestPseudo')
    compte_dao.delete(compte_dao.get('TestPseudo'))


def test_valid_pseudo_inconnu():
    assert compte_dao.pseudo_disponible('PseudoFantaisiste')

def test_valid_mot_de_passe():
    compte_dao.create('TestPseudo', 'MotDePassePseudo')
    assert compte_dao.get('TestPseudo').password ==  'MotDePassePseudo'
    compte_dao.delete(compte_dao.get('TestPseudo'))

def test_invalid_mot_de_passe():
    compte_dao.create('TestPseudo', 'MotDePassePseudo')
    assert compte_dao.get('TestPseudo').password !=  'password'
    compte_dao.delete(compte_dao.get('TestPseudo'))
