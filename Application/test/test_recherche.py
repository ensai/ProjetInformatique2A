from dao.filtre_de_recherche import Filtre_de_recherche

def test_definir_recherche():
    #on teste une recherche quelconque
    filtre = Filtre_de_recherche()
    filtre.definir_recherche("Voiture")
    assert filtre.recherche == "Voiture"

    #on teste un changement de recherche
    filtre.definir_recherche("Téléphone")
    assert filtre.recherche != "Voiture"
    assert filtre.recherche == "Téléphone"



def test_definir_categorie():
    #on teste une catégorie quelconque
    filtre = Filtre_de_recherche()
    filtre.definir_categorie("Véhicules")
    assert filtre.categorie == "Véhicules"

    #on teste un changement de catégorie
    filtre.definir_categorie("Multimédia")
    assert filtre.categorie != "Véhicules"
    assert filtre.categorie == "Multimédia"



def test_definir_prix():
    #on teste un prix quelconque
    filtre = Filtre_de_recherche()
    filtre.definir_prix("10")
    assert filtre.prix == "10"

    #on teste un changement de prix
    filtre.definir_prix("100")
    assert filtre.prix != "10"
    assert filtre.prix == "100"



def test_definir_localisation():
    #on teste une localisation (région) quelconque
    filtre = Filtre_de_recherche()
    filtre.definir_localisation("Bretagne")
    assert filtre.localisation == "Bretagne"

    #on teste un changement de localisation
    filtre.definir_localisation("Ile-de-France")
    assert filtre.localisation != "Bretagne"
    assert filtre.localisation == "Ile-de-France"



def test_definir_critere():
    #on teste un critère quelconque
    filtre = Filtre_de_recherche()
    filtre.definir_critere("Prix")
    assert filtre.critere == "Prix"

    #on teste un changement de critère
    filtre.definir_critere("Localisation")
    assert filtre.critere != "Prix"
    assert filtre.critere == "Localisation"


def test_definir_ordre():
    #on teste un ordre quelconque
    filtre = Filtre_de_recherche()
    filtre.definir_ordre("Croissant")
    assert filtre.ordre == "Croissant"

    #on teste un changement d'ordre
    filtre.definir_ordre("Décroissant")
    assert filtre.ordre != "Croissant"
    assert filtre.ordre == "Décroissant"
    

def test_definir_code_postal():
    #on teste un code postal quelconque
    filtre = Filtre_de_recherche()
    filtre.definir_code_postal("35170")
    assert filtre.code_postal == "35170"

    #on test un changement de code postal
    filtre.definir_code_postal("88120")
    assert filtre.code_postal != "35170"
    assert filtre.code_postal == "88120"



def test_getitem():
    filtre = Filtre_de_recherche()


    filtre.definir_critere("Prix")
    assert filtre["sort"] == "Prix"

    filtre.definir_ordre("Croissant")
    assert filtre["order"] == "Croissant"

    filtre.definir_categorie("Véhicules")
    assert filtre["category"] == "Véhicules"

    filtre.definir_recherche("Voiture")
    assert filtre["text"] == "Voiture"

    filtre.definir_localisation("Bretagne")
    assert filtre["region"] == "Bretagne"

    filtre.definir_prix("10")
    assert filtre["price"] == "10"

    filtre.definir_code_postal("35170")
    assert filtre["location"] == "35170"