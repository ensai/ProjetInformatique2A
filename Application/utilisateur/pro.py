# -*- coding: utf-8 -*-
"""
Éditeur de Spyder

Ceci est un script temporaire.
"""
from dao.compte_dao import CompteDao
from utilisateur.particulier import Particulier
class Pro(Particulier):
    
    def __init__(self,username,password,sexe=None,age=None,nom=None,prenom=None,adresse=None,nom_entreprise=None,secteur=None,telephone=None):
         Particulier. __init__(self,username,password,sexe,age,nom,prenom,adresse)
         self.nom_entreprise=nom_entreprise
         self.secteur=secteur
         self.telephone=telephone
    
    def __str__(self):
        resu = Particulier.__str__(self)
        if self.nom_entreprise:
            resu += "\nnom_entreprise:"+ str(self.nom_entreprise)
        if self.secteur:
            resu += "\nsecteur:"+ str(self.secteur)
        if self.telephone:
            resu += "\ntelephone:"+ str(self.telephone)
        return resu
    
    def change_phone(self,numero):
        self.telephone = numero
        CompteDao.update(self.username,"telephone",numero)
    def change_secteur(self,activité):
        self.secteur = activité
        CompteDao.update(self.username,"secteur", activité)
    def change_nom_entreprise(self,name_entreprise):
        self.nom_entreprise = name_entreprise
        CompteDao.update(self.username,"nom_entreprise", name_entreprise)
        
        
    def type_d_utilisateur(self):
        return "Professionnel"
        
