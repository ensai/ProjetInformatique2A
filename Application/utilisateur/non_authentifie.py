from dao.compte_dao import CompteDao

class Utilisateur:

    def se_connecter(self,pseudo,motdepasse):
        compte = CompteDao.get(pseudo)
        if compte:
            return compte
        else:
            print("erreur dans le pseudonyme/le mot de passe")
            return compte