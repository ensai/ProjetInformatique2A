from dao.compte_dao import CompteDao
from dao.annonce import AnnonceDao
from utilisateur.pro import Pro

class Admin(Pro):

     def supprimer_annonce(self,id_ann):
         AnnonceDao.delete(id_ann)

     def supprimer_utilisateur(self,username):
         CompteDao.delete(CompteDao.get(username))
         
     def type_d_utilisateur(self):
         return "Administrateur"