from dao.compte_dao import CompteDao
from dao.liste_d_envies import Liste_d_envies_DAO
from dao.historique import HistoriqueDAO

class Particulier:

    def __init__(self,username,password,sexe=None,age=None,nom=None,prenom=None,adresse=None ):
        self.username = username
        self.age = age
        self.sexe = sexe
        self.password = password
        self.nom = nom
        self. prenom = prenom
        self. adresse = adresse

    def type_d_utilisateur(self):
        return "Particulier"

    def __str__(self):
        resu = "username:"+ str( self.username)
        if self.age:
            resu += "\nage:"+ str(self.age)
        if self.nom:
            resu += "\nnom:" + str (self.nom)
        if self.prenom:
            resu += "\nprenom:" + str (self.prenom)
        if self.adresse:
            resu += "\nadresse:" + str ( self.adresse)
        return resu

    def supprimer_compte(self):
        CompteDao.delete(self)
        HistoriqueDao.delete(self.username)
        liste_d_envies_DAO.delete_user(self.username)

    def change_password(self,password):
        self.password = password
        CompteDao.update(self.username,"motdepasse",password)

    def change_sexe(self,sexe):
        self.sexe = sexe
        CompteDao.update(self.username,"sexe",sexe)

    def change_nom(self,nom):
        self.nom = nom
        CompteDao.update(self.username,"nom",nom)

    def change_prenom(self,prenom):
        self.prenom = prenom
        CompteDao.update(self.username,"prenom",prenom)

    def change_adresse(self,adresse):
        self.adresse = adresse
        CompteDao.update(self.username,"adresse",adresse)

    def change_age(self,age):
        self.age = age
        CompteDao.update(self.username,"age",age)
