import requests
from unidecode import unidecode
from env import headers_leboncoin,headers_google,headers_vivastreet

class Get:

    def __init__(self,url,site,proxies=None):
        self.url = url
        self.site = site
        self.proxies = proxies

    def get_page(self):
        try:
            if self.proxies and self.site == "www.leboncoin.fr":
                response = requests.get(self.url, proxies=self.proxies, headers=headers_leboncoin) #ensemble de l'header
            elif self.site == "www.leboncoin.fr":
                response = requests.get(self.url, headers=headers_leboncoin)
            elif self.proxies and (self.site == "www.vivastreet.com" or self.site == "search.vivastreet.com"):
                response = requests.get(self.url, proxies=self.proxies, headers=headers_vivastreet) #ensemble de l'header
            elif self.site == "www.vivastreet.com" or self.site == "search.vivastreet.com":
                response = requests.get(self.url, headers=headers_vivastreet)
            elif self.site == "www.google.fr":
                response = requests.get(self.url, headers=headers_google)
            else:
                raise
            #interrompt si la requête ne revoit pas un 200's
            response.raise_for_status()
            return response.content
        except requests.exceptions.RequestException as error:
            print(unidecode(str(error)))
            if str(error)[:3]=="410":
                return "La page n'existe plus"
