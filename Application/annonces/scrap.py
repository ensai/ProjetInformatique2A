from lxml import html
import re
from unidecode import unidecode
from annonces.get_page import Get
from annonces.post_phone import Post


class Scrap:

    def __init__(self,url,proxies=None):
        self.site = re.search("(www|search)\..*\.(com|fr)",url).group(0)
        get = Get(url,self.site,proxies)
        html_doc = get.get_page()
        if html_doc != "La page n'existe plus":
            self.tree = html.fromstring(html_doc)
        else:
            self.tree = None


    #On repère les différentes informations par leur xpath dans le document
    def get_title(self):
        if self.tree is not None:
            return unidecode(self.tree.xpath('//title/text()')[0])
        else:
            return "La page n'existe plus"

    def get_description(self):
        if self.site == "www.leboncoin.fr":
            return self.get_description_leboncoin()
        elif self.site == "www.vivastreet.com":
            return self.get_description_vivastreet()

    def get_description_leboncoin(self):
        return unidecode("\n".join(self.tree.xpath('//*[@data-qa-id="adview_description_container"]/div/span/text()')))

    def get_description_vivastreet(self):
        return unidecode("\n".join(self.tree.xpath('//*[@class="shortdescription"]/text()')))


    def get_price(self):
        if self.site == "www.leboncoin.fr":
            return self.get_price_leboncoin()
        elif self.site == "www.vivastreet.com":
            return self.get_price_vivastreet()

    def get_price_leboncoin(self):
        string = '//*[@id="container"]/main/div/div/div/section/section/section[1]/div[1]/div[2]/div[1]/div[2]/div/span/text()'
        resu = self.tree.xpath(string)
        return unidecode(resu[0]+resu[1])

    def get_price_vivastreet(self):
        return unidecode(self.tree.xpath('//*[@id="title_price"]/text()')[0])

    def get_list(self):
        # a expliciter
        if self.site == "www.leboncoin.fr":
            return self.get_list_leboncoin()
        elif self.site == "search.vivastreet.com":
            return self.get_list_vivastreet()

    def get_list_leboncoin(self):
        return self.tree.findall('.//li/a/section/p/span')

    def get_list_vivastreet(self):
        return self.tree.findall('.//a/div[2]/div[2]/h4')

    def get_seller(self):
        if self.site == "www.leboncoin.fr":
            return self.get_seller_leboncoin()
        elif self.site == "www.vivastreet.com":
            return self.get_seller_vivastreet()

    def get_seller_leboncoin(self):
        string = './/section[2]/div[1]/div/div/div/div[1]/div[2]/div/text()'
        return unidecode(self.tree.xpath(string)[0])

#sur viva street
# regarder si ça marche.
    def get_seller_vivastreet(self):
        string = '//*[@id="classified-detail-block"]/div[1]/span/span[1]/text()'
        return unidecode(self.tree.xpath(string)[0])


    def get_date(self):
        if self.site == "www.leboncoin.fr":
            return self.get_date_leboncoin()
        elif self.site == "www.vivastreet.com":
            return self.get_date_vivastreet()

    def get_date_leboncoin(self):
        string = './/section[1]/div[1]/div[2]/div[1]/div[3]/text()'
        return unidecode(self.tree.xpath(string)[0])

#sur viva street
# même problème que pour get_seller_viva
    def get_date_vivastreet(self):
        string = '//*[@id="classified-detail-block"]/div[1]/span/span[2]/text()'
        return unidecode(self.tree.xpath(string)[0])

    def get_localisation(self):
        if self.site == "www.leboncoin.fr":
            return self.get_localisation_leboncoin()
        elif self.site == "www.vivastreet.com":
            return self.get_localisation_viva()

    def get_localisation_leboncoin(self):
        string = './/div[2]/div/div/div[2]/span/text()'
        resu = self.tree.xpath(string)
        return unidecode(resu[0]+"  "+resu[2])

#sur viva street
    def get_localisation_viva(self):
        string = './/tr[2]/td[2]/div/text()'
        resu = self.tree.xpath(string)
        return unidecode(resu[-1])


    def can_get_phone_number(self):
        if self.site == "www.leboncoin.fr":
            return self.can_get_phone_number_leboncoin()
        elif self.site == "www.vivastreet.com":
            return self.can_get_phone_number_vivastreet()

    def can_get_phone_number_leboncoin(self):
        string = './/div[3]/div[1]/div/div/button/span'
        resu = self.tree.xpath(string)
        if resu and resu[0].get("name")=="phone":
            return "Numéro masqué"
        else:
            return "Absence de coordonnées de contacts"

#à refaire toute la suite
    def can_get_phone_number_vivastreet(self):
        string = '//*[@id="contact_phone_bottom_wrapper"]'
        resu = self.tree.xpath(string)
        if resu :
            return resu[0].get("data-phone-number")
        else:
            return "Absence de coordonnées de contacts"

    def get_photo(self):
        photos =  self.tree.findall('.//div[2]/img')
        resu = list()
        for x in photos:
            resu.append(x.get("src"))

#sur viva street
    def get_photo_viva(self):
        photos = self.tree.findall('//*[@id="vs_photos_0"]/img')
        resu = list()
        for x in photos:
            resu.append(x.get("src"))

    #A l'exception du numéro de téléphone pour lequel on doit faire appel à l'api de leboncoin
    def get_phone_number_leboncoin(url,proxies=None):
        #l'id est contenu dans l'url de l'annonce
        post = Post(url,proxies)
        response = post.post()
        try:
            return unidecode(response["utils"]["phonenumber"])
        except:
            return "bloque par leboncoin, reesayez plus tard."


    def get_my_position(self):
        code_postal = self.tree.xpath('//*[@id="geolocation_address"]')
        return code_postal
