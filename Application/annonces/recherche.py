from annonces.scrap import Scrap
try:
    from env import proxies
except:
    pass
from env import elements_recherche,liste_sites,categories_viva,localisation_viva
from unidecode import unidecode

class Recherche:

    def __init__(self,filtre):
        self.resultats_dico = {}
        for site in liste_sites:
            self.recherche(site,filtre)
        self.organise(filtre)

    def organise(self,filtre):
        liste_pour_trier = list()
        for site in self.resultats_dico:
                for annonce in self.resultats_dico[site]:
                    liste_pour_trier.append((annonce,self.get_price(annonce,site),self.get_date(annonce,site),site))
        if filtre["sort"] == "time":
            liste_pour_trier.sort(key=lambda donnees: donnees[2][::-1] , reverse = filtre["order"] != "asc")
        elif filtre["sort"] == "price":
            liste_pour_trier.sort(key=lambda donnees: int(donnees[1].replace(" ","")[:-3]) , reverse = filtre["order"] != "asc")
        self.resultats = liste_pour_trier


    def get_date(self,annonce,site):
        if site == "leboncoin":
#            if len(annonce.getparent().getparent()[2])==2 and len(annonce.getparent().getparent()[2][1])==2:
#                return annonce.getparent().getparent()[2][1][1].get("content")
#            elif len(annonce.getparent().getparent()[2])==2:
#                return annonce.getparent().getparent()[2][1][0].get("content")
#            elif len(annonce.getparent().getparent()[2][0])==2:
#                return annonce.getparent().getparent()[2][0][1].get("content")
#            else:
#                return annonce.getparent().getparent()[2][0][0].get("content")
            from annonces.annonce import Annonce
            return Annonce("https://www.leboncoin.fr"+annonce.getparent().getparent().getparent().get("href")).date[:10]
        elif site == "Vivastreet":
            from annonces.annonce import Annonce
            return Annonce(annonce.getparent().getparent().getparent().get("href")).date[3:13]


    def get_price(self,annonce,site):
        if site == "leboncoin":
            if len(annonce.getparent().getparent()[2])==2:
                return unidecode(annonce.getparent().getparent()[2][0][1][0].text_content())
            else:
                return "Pas de prix"
        elif site == "Vivastreet":
            return unidecode(annonce.getparent().getparent()[3][0].text_content())

    def recherche(self,site,filtre):
        if site == "leboncoin":
            url = "https://www.leboncoin.fr/recherche/?"
            for x in elements_recherche:
                try:
                    #le premier ajout ne doit pas commencer par &
                    if url[-1] == '?':
                        url = url + x + "=" + filtre[x]
                    else: #contrairement à ceux d'après
                        url = url + "&" + x + "=" + filtre[x]
                except:
                    pass

        elif site == "Vivastreet" :
            url = "http://search.vivastreet.com/"+categories_viva[filtre["category"]]+"/"+localisation_viva[filtre["region"]]+"?start_field=1"
            for x in elements_recherche:
                try:
                    if not (x=="category" or x=="region"):
                        url = url + "&" + elements_recherche_viva[x] + "=" + transformation_viva[filtre[x]]
                except:
                    pass
        try:
            scrap = Scrap(url,proxies)
        except:
            scrap = Scrap(url)
        self.resultats_dico[site] = scrap.get_list()
