import requests
import json
from env import headers_api_leboncoin,data_phone_leboncoin,url_phone_leboncoin

class Post:

    def __init__(self,url,proxies=None):
        self.headers = headers_api_leboncoin
        self.headers["Referer"] = url
        self.data = data_phone_leboncoin
        #l'information list_id est la seule information qui diffère d'une annonce à l'autre
        self.data["list_id"] = url[-15:-5]
        self.proxies = proxies

    def post(self):
        try:
            if self.proxies:
                response = requests.post(url_phone_leboncoin, proxies=self.proxies, headers=self.headers,data = self.data)
            else:
                response = requests.post(url_phone_leboncoin, headers=self.headers, data = self.data)
            #interrompt si la requête ne revoit pas un 200's
            response.raise_for_status()
            return json.loads(response.content.decode('utf-8')) #le retour est transformé en dictionnaire
        except requests.exceptions.RequestException as error:
            print(error)
