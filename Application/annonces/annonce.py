from annonces.scrap import Scrap
from dao.annonce import AnnonceDao
#Si l'application n'a pas besoin de passer par un proxy, il suffit de supprimer la variable proxies du fichier env
try:
    from env import proxies
except:
    pass
import re


class Annonce:

    def __init__(self,url=None):
        self.id = None
        self.url = url

        #Si l'annonce est blacklistée alors url is None
        if url:
            #On ajoute l'annonce dans la base de données au besoin et on définit self.id
            AnnonceDao.create(self)

            #On couvre les cas avec ou sans proxy
            try:
                scrap = Scrap(url,proxies)
            except:
                scrap = Scrap(url)

        #Permet de couvrir tous les cas : si l'acquisition n'est pas possible alors on a une chaîne vide
        try:
            self.numero = scrap.can_get_phone_number()
        except:
            self.numero = ""
        try:
            self.titre = scrap.get_title()
        except:
            self.titre = "Annonce supprimée par un administrateur"
        try:
            self.prix = scrap.get_price()
        except:
            self.prix = ""
        try:
            self.description = scrap.get_description()
        except:
            self.description = ""
        try:
            self.localisation = scrap.get_localisation()
        except:
            self.localisation = ""
        try:
            self.vendeur = scrap.get_seller()
        except:
            self.vendeur = ""
        try:
            self.date = scrap.get_date()
        except:
            self.date = ""

    def get_numero(self):
        #extrait le site de l'url grâce à une expression régulière
        site = re.search("www\..*\.(com|fr)",self.url).group(0)
        #On ne va chercher le numéro que dans le cas de leboncoin
        if site == "www.leboncoin.fr":
            #On ne va chercher pas chercher le numéro s'il n'existe pas
            if self.numero != "Absence de coordonnées de contacts":
                self.numero = AnnonceDao.get_numero(self)
                #On regarde s'il est déjà dans la base de données
                if self.numero == "None":
                    #Si non, on le récupère
                    try:
                        self.numero = Scrap.get_phone_number(self.url,proxies)
                    except:
                        self.numero = Scrap.get_phone_number(self.url)
                    finally:
                        #Puis on l'enregistre en base si on a réussit à le récupérer
                        if self.numero != "bloque par leboncoin, reesayez plus tard.":
                            AnnonceDao.post_numero(self)

    def __eq__(self, other):
        return self.id == other.id

    def __str__(self):
        #Différentes informations lignes après lignes
        string = "Titre : "+self.titre
        if self.prix:
            string += "\nPrix : "+self.prix
        if self.description:
            string += "\nDescription : "+self.description
        if self.localisation:
            string +="\nLocalisation : "+self.localisation
        if self.vendeur:
            string +="\nVendeur : "+self.vendeur
        if self.numero:
            string +="\nContact : "+self.numero
        if self.date:
            string +="\nDate de mise en ligne : "+self.date
        return string
