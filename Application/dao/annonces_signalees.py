from dao.connection import connection
import psycopg2


class SignalementAnnonceDAO:

    def create(id_annonce, username, motif, date):
        cur = connection.cursor()
        try:
            cur.execute("insert into signalement (id_annonce, username, motif, date) values (%s,%s,%s,%s);", (id_annonce, username, motif, date))

            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()

    def delete(id_annonce, username):
        cur = connection.cursor()
        try:
            cur.execute(
                "delete from signalement where id_annonce=%s and username=%s", (id_annonce, username))
            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()

    def get():
        with connection.cursor() as cur:
            cur.execute("select id_annonce, username, motif, date from signalement")
            found = cur.fetchall()
            return found
