class Filtre_de_recherche:

#J'ai rajouté en entré ordre afin de permettre à l'utilisateur de choisir s'il préfère choisir par ordre croissant ou decroissant le critere qui tri.
#Critere = popularité, prix, localisation (le plus proche)
    def definir_recherche(self, recherche):
        self.recherche = recherche

    def definir_categorie(self, categorie):
        self.categorie = categorie

    def definir_prix(self, prix):
        self.prix = prix

    def definir_localisation(self, localisation):
        self.localisation = localisation

    def definir_critere(self, critere):
        self.critere = critere

    def definir_ordre(self, ordre):
        self.ordre = ordre

    def definir_code_postal(self, code_postal):
        self.code_postal = code_postal

    def definir_recherche_pro(self,booleen):
        if booleen:
            self.recherche_pro = "pro"
        else:
            self.recherche_pro = ""

    def __getitem__(self, item):
        if item == "sort":
            return self.critere
        elif item == "order":
            return self.ordre
        elif item == "category":
            return self.categorie
        elif item == "text":
            return self.recherche
        elif item == "region":
            return self.localisation
        elif item == "price":
            return self.prix
        elif item == "location":
            return self.code_postal
        elif item == "owner_type":
            return self.recherche_pro
        else:
            raise AttributeError()
