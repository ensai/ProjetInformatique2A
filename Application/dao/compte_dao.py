from dao.connection import connection
import psycopg2


class CompteDao:

    def create(pseudo, motdepasse, type_utilisateur="Particulier"):
        cur = connection.cursor()
        try:
            cur.execute(
                "insert into account (pseudo, motdepasse,type_utilisateur) values (%s, %s,%s);", (pseudo, motdepasse,type_utilisateur))

            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()

    def delete(compte):
        cur = connection.cursor()
        try:
            cur.execute(
                "delete from account where pseudo=%s", (compte.username,))

            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()

    def get(pseudo):
        with connection.cursor() as cur:
            cur.execute(
                "select pseudo, motdepasse, sexe, age, nom, prenom, adresse , nom_entreprise,secteur,telephone, type_utilisateur from account where pseudo=%s", (pseudo,))

            found = cur.fetchone()
            if found and found[10] == "Particulier":
                from utilisateur.particulier import Particulier
                return Particulier(found[0], found[1],found[2],found[3],found[4],found[5])
            if found and found[10] == "Professionnel":
                from utilisateur.pro import Pro
                return Pro(found[0], found[1],found[2],found[3],found[4],found[5],found[6],found[7],found[8],found[9])

            elif found and found[10] == "Administrateur":
                from utilisateur.admin import Admin
                return Admin(found[0], found[1],found[2],found[3],found[4],found[5],found[6],found[7],found[8],found[9])
            else:

                 return None

    def list_pseudo():
        def get():
            with connection.cursor() as cur:
                cur.execute("select pseudo, type_utilisateur from account")
                found = cur.fetchall()
                return found

    def update(pseudo,colonne,valeur):
        cur = connection.cursor()
        try:
            cur.execute(
                "update account set "+colonne+" = %s where pseudo=%s", (valeur,pseudo))

            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()

    def pseudo_disponible(pseudo):
        return not(CompteDao.get(pseudo))
