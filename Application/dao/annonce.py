from dao.connection import connection
import psycopg2


class AnnonceDao:

    def create(annonce):
        cur = connection.cursor()
        try:
            cur.execute("select id from annonces where url=%s", (annonce.url,))
            found = cur.fetchone()
            if not found:
                cur.execute("insert into annonces (url, blacklist, numero) values (%s,%s,%s) returning id;", (annonce.url,"0","None"))
                annonce.id = cur.fetchone()[0]
            else:
                annonce.id = found[0]
            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()

    def get_numero(annonce):
        cur = connection.cursor()
        try:
            cur.execute("select numero from annonces where id=%s", (annonce.id,))
            found = cur.fetchone()
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()
        return found[0]

    def post_numero(annonce):
        cur = connection.cursor()
        try:
            cur.execute("update annonces set numero = %s where id=%s", (annonce.numero,annonce.id))
            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()


    def delete(id):
        cur = connection.cursor()
        try:
            cur.execute(
                "delete from annonces where id=%s", (id,))
            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()

    def get(id):
        with connection.cursor() as cur:
            cur.execute(
                "select id, url, blacklist from annonces where id=%s", (id,))
            found = cur.fetchone()
            if found and not found[2]:
                from annonces.annonce import Annonce
                annonce = Annonce(found[1])
                return annonce
            else:
                from annonces.annonce import Annonce
                annonce = Annonce()
                return annonce

    def blacklist(id):
        cur = connection.cursor()
        try:
            cur.execute(
                "select blacklist from annonces where id=%s", (id,))
            if cur.fetchone()[0]:
                cur.execute("update annonces set blacklist = '0' where id=%s", (id,))
            else:
                cur.execute("update annonces set blacklist = '1' where id=%s", (id,))
            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()
