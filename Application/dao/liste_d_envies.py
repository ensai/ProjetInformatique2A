
from dao.connection import connection
import psycopg2

class Liste_d_envies_DAO:

    def create(nom_liste,id_annonce,username):
        cur = connection.cursor()
        try:
            cur.execute(
                    "INSERT INTO liste (nom_liste,id_annonce, username) VALUES (%s,%s, %s);", (nom_liste,id_annonce,username))

            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()


    def delete_liste(username,nom):
        cur = connection.cursor()
        try:
            cur.execute(
                "delete from liste where username = %s and nom_liste = %s", (username,nom))

            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()

    def delete(id_ann,username,nom):
        cur = connection.cursor()
        try:
            cur.execute(
                "delete from liste where id_annonce=%s and username = %s and nom_liste = %s", (str(id_ann),username,nom))

            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()

    def delete_user(username):
        cur = connection.cursor()
        try:
            cur.execute(
                "delete from liste where username = %s", (username,))

            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()

    def lister(nom,username):
        cur = connection.cursor()
        try:
            cur.execute(
                "select id_annonce from liste where nom_liste=%s and username = %s",(nom,username))

            # la transaction est enregistrée en base
            id_annonces = cur.fetchall()
            if id_annonces:
                return id_annonces
            else:
                return list()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()

    def recherher_ann(liste):
        cur = connection.cursor()
        try:
            cur.execute(
                "select * from liste where id_annonce=%s and username=%s", (liste.id_ann,list.username))

            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()

    def ensemble_listes(username):
        cur = connection.cursor()
        try:
            cur.execute(
                "select distinct nom_liste from liste where username=%s", (username,))

            # la transaction est enregistrée en base
            found = cur.fetchall()
            connection.commit()
            if found:
                return found
            else:
                return list()

        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()
