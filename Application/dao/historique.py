from dao.connection import connection
import psycopg2

class HistoriqueDAO:


    def enregistrer_dans_base(donnees):
        id_annonce = donnees[2]
        cur=connection.cursor()
        cur.execute("Select id from annonces where id = '"+str(id_annonce)+"';")
        rows=cur.fetchall()
        cur.close()
        if len(rows)!=0 :
            cur=connection.cursor()
            cur.execute("INSERT INTO historique (username,date,recherche) Values (%s,%s,%s)", donnees)
            connection.commit()
            cur.close()
        else :
            return("Error id d'annonces non présent dans la liste d'annonces utilisé")

    def get(historique,identifiant):
        historique.liste_couple_id_date = list()
        cur=connection.cursor()
        cur.execute("select * from historique where username=%s ",(identifiant,))
        rows = cur.fetchall()
        rows.sort(key=lambda x : x[1], reverse=True)
        cur.close()
        if len(rows) != 0:
            for k in rows:
                historique.liste_couple_id_date.append((k[3],k[2]))

    def drop(username):
        cur = connection.cursor()
        try:
            cur.execute(
                "delete from historique where username = %s", (username,))
            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()

    def delete(couple_id_date,username):
        cur = connection.cursor()
        try:
            cur.execute(
                "delete from historique where username = %s and recherche = %s and date = %s", (username,couple_id_date[0],couple_id_date[1]))
            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()
