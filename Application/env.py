﻿proxies = {'http': 'http://pxcache-02.ensai.fr:3128','https': 'http://pxcache-02.ensai.fr:3128'}

url_phone_leboncoin = "https://api.leboncoin.fr/api/utils/phonenumber.json"

data_phone_leboncoin = {
"app_id" : "leboncoin_web_utils",
"key" : "54bb0281238b45a03f0ee695f73e704f",
"text" : "1",
"list_id" : "",
}

liste_sites = ["leboncoin","Vivastreet"]

headers_leboncoin = {
'Host': "www.leboncoin.fr",
"Connection": "keep-alive",
"Cache-Control": "max-age=0",
"Upgrade-Insecure-Requests": "1",
"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36",
"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
"Referer": "https://www.leboncoin.fr/",
"Accept-Encoding": "gzip, deflate, br",
"Accept-Language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7",
}

headers_api_leboncoin = {
'Host': 'api.leboncoin.fr',
'Connection': 'keep-alive',
'Content-Length': '89',
'Accept': 'application/json',
'Origin': 'https://www.leboncoin.fr',
'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',
'Content-Type': 'application/x-www-form-urlencoded',
'Referer': '',
'Accept-Encoding': 'gzip, deflate, br',
'Accept-Language': 'fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7',
}

headers_ma_position= {
}

elements_recherche = ["text","region","category","order","sort","location","price","owner_type"]

categories = {"emploi":"71","vehicules":"1","immobilier":"8","vacances":"66","multimedia":"14","loisirs":"24",
            "materiel professionnel":"56","services":"31","maison":"18","autre":"37" }

localisation = {"Nord-Pas-de-Calais":"17","Picardie":"19","Haute-Normandie":"11","Ile-de-France":"12",
                "Centre":"7","Pays de la Loire":"18","Basse-Normandie":"4","Bretagne":"6","Poitou-Charentes":"20",
                "Limousin":"14","Aquitaine":"2","Midi-Pyrénées":"16","Languedoc-Roussillon":"13","Auvergne":"3",
                "Provence-Alpes-Côte d'Azur":"21","Corse":"9","Rhône-Alpes":"22","Bourgogne":"5",
                "Franche-Comté":"10","Champagne-Ardenne":"8","Lorraine":"15","Alsace":"1"}
MY_ID = "id1087"

#les éléménts de recherche, les catégories et les codes postaux sur vivastreet.

elements_recherche_viva={"order":"order","sort":"sort","text":"keyword","owner_type":"individual_type"}

transformation_viva={"price":"sp_common_price","time":"posted","asc":"a","desc":"d","pro":"pro"}

## plus de catégorie sur viva street
categories_viva = {"71":"emploi-annonces","1":"autos-motos-bateaux","8":"annonces-immobilier",
                "14":"vente-multimedia","56":"engins-materiels-pro","31":"services",
                "18":"vente-maison","37":"tous-autres","66":"achat-vente-billets-concerts","24":"vente-loisirs"}

localisation_viva={"1":"alsace","2":"aquitaine","3":"auvergne","4":"basse-normandie",
                    "5":"bourgogne","6":"bretagne","7":"centre","8":"champagne-ardenne",
                    "9":"corse","DOM-TOM":"dom-tom","10":"franche-comte","11":"haute-normandie",
                    "12":"ile-de-france","13":"languedoc-roussillon","14":"limousin",
                    "15":"lorraine","16":"midi-pyrenees","17":"nord-pas-de-calais",
                    "18":"pays-de-la-loire","19":"picardie","20":"poitou-charentes",
                    "21":"paca","22":"rhone-alpes"}

headers_vivastreet = {
"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
"Accept-Encoding": "gzip, deflate",
"Accept-Language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7",
"Cache-Control": "max-age=0",
"Proxy-Connection": "keep-alive",
"Referer": "https://www.google.fr/",
"Upgrade-Insecure-Requests": "1",
"User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.67 Safari/537.36"
}

headers_google = {
":authority": "www.google.fr",
":method": "GET",
":path": "/search?q=ma+position",
":scheme": "https",
"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
"accept-encoding": "gzip, deflate, br",
"accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7",
"cache-control": "max-age=0",
"upgrade-insecure-requests": "1",
"user-agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36",
}
