import json
import os.path
from annonces.annonce import Annonce
from dao.liste_d_envies import Liste_d_envies_DAO


class Importer:
    def importer_liste(path):
        fichier=open(path)
        data=json.load(fichier)
        for dico in data:
            annonce=Annonce(dico["annonce"]["url"])
            Liste_d_envies_DAO.create(dico["Nom_liste"],annonce.id,dico["utilisateur"])
        fichier.close()
