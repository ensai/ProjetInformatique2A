import json
from dao.annonce import AnnonceDao
import os.path

class Export:
    def format_annonce(annonce):
        return {"Titre" :annonce.titre ,"Prix" : annonce.prix ,"Description" :annonce.description ,"Localisation" : annonce.localisation ,"Vendeur" : annonce.vendeur,"Date de mise en ligne " :annonce.date,"url":annonce.url}


    def exporter(dictionnaire,fichier):
        j= json.dumps(dictionnaire)
        fichier.write('[\n')
        fichier.write(j)
        fichier.write('\n]')
        fichier.close()

    def ajouter(dictionnaire,path):
        lines=[]
        j= json.dumps(dictionnaire)
        fichier=open(path,"r")
        for l in fichier.readlines():
            if l!="]":
                lines.append(l)

        fichier = open(path, 'w')
        fichier.write("".join(lines))
        fichier.write(",\n")
        fichier.write(j)
        fichier.write('\n]')
        fichier.close()
#        print("l'annonce a ete rajoutee dans le fichier existant")





    def exporter_annonce(annonce,path):
        if not os.path.isfile(path):
            fichier=open(path,"w")
            Export.exporter(Export.format_annonce(annonce),fichier)
        else:
            Export.ajouter(Export.format_annonce(annonce),path)



    def exporter_list(list,path):
        for id in list.id_ann:
            annonce=AnnonceDao.get(id)
            dict={"Nom_liste" :list.nom ,"utilisateur":list.username,"annonce":Export.format_annonce(annonce)}
            if not os.path.isfile(path):
                fichier=open(path,"w")
                Export.exporter(dict,fichier)
            else:
                Export.ajouter(dict,path)



    def exporter_historique(historique,path):
        for id_date in historique.liste_couple_id_date:
            annonce=AnnonceDao.get(id_date[0])
            dict={"annonce" :Export.format_annonce(annonce) ,"utilisateur":historique.username,"date/heure_annonce":id_date[1]}
            if not os.path.isfile(path):
                fichier=open(path,"w")
                Export.exporter(dict,fichier)
            else:
                Export.ajouter(dict,path)
