# Projet Informatique 2A

Projet informatique pour la deuxième année de l'ensai.

Le projet consiste à implémenter application enligne de commande pour faciliter les achats sur internet.
Votre startup s’appelle **\_\_\_** et votre application GPA.

## Fonctionnalités requises (numérotées, mais non ordonnées)

- F1 : la recherche de résultats sur un site que vous choisirez (Leboncoin, Vivastreet, …)
- F2 : authentification et gestion du profil utilisateur/profil administrateur
- F3 : gestion de l’import/export de données au format choisi
- F4 : gestion de l’historique des recherches
- F5 : recherche par catégorie/filtre de recherches

## Fonctionnalités optionnelles

- FO1 : lancer des recherches sur plusieurs sites en parallèle
- FO2 : géo localisation de l’utilisateur et des annonces/distance entre l’utilisateur et l’annonce
- FO3 : moteur de recommandations
- FO4 : … à vous de jouer !
- NOTE : ce sujet regorge de «…». En effet, une participation active est vivement recommandée et vos idées, pour rendre votre projet unique, sont les bienvenues !

mail : colin.leverger@irisa.fr

Comme vous avez pu constater (je l’espère du moins…) il est difficile de faire des requêtes pour récupérer les pages du site leboncoin.fr en utilisant uniquement « request » : le site repère que vous êtes un robot… et vous renvoie le code d’erreur « 403 » plutôt que les résultats HTML du site !

Deux solutions à ça :

1.  Utiliser la librairie Scrapy ; malheureusement, il me semble que les environnements de l’ENSAI sont encore bien vérouillés sur ce point pour installer le package. cette solution ne serait donc possible que si vous travaillez sur vos ordinateurs personnels. La manipulation d’installation est la suivante (pour ceux qui veulent tester à l’ENSAI) :

    > conda create -y -n scrapy-env
    >
    > cd « .conda/envs/scrapy-env/Scripts »
    >
    > activate conda-env
    >
    > conda install -c conda-forge scrapy
    >
    > En fait, pour faire une install en local il faut d’abord créer un environnement virtuel python et l’installer dans cet environnement (ce qui veut dire qu’il n’est disponible que dans cet environnement). Ce sont des notions complexes... Et peut être à éviter pour ce projet !

2.  Utiliser un autre site (vivastreet, etc.) pour faire les requêtes

RESSOURCES :

- Veuillez trouver ci-joint un exemple commenté format HTML, des requêtes, de l’utilisation de scrapy, et un panel de sites marchands type leboncoin mais accessibles depuis Python (je ne m’occupe en aucun cas du parsing, ça sera à vous de jouer, juste des exemples de requêtes qui passent ;-))
- Voici un tutoriel intéressant et bien guidé sur comment scrapper en Python : https://pythontips.com/2018/06/20/an-intro-to-web-scraping-with-lxml-and-python/

REMARQUES (important!) :

- Vous êtes nombreux à me le souligner : faire du scraping n’est pas forcément très légal. Dans notre cas, c’est à but pédagogique, on ne risque pas grand-chose… Mais je vous invite à prendre des précautions pour éviter les problèmes (comme dans tout projet de scraping):

  > petit temps d’attente entre chaque requête
  >
  > sauvegarder des résultats HTML et utiliser les résultats sur disque/base de donnée/etc... pour vos tests logiciels et vos scénarios !
  >
  > etc...
  >
  > ne pas sauvegarder l'intégralité d'un site ! (exemple : si vous cherchez une voiture, il n'est pas obligatoire de récupérer les 120 pages du site cible ! peut être que deux ou trois pages suffisent ?)

- Au niveau de l’abstraction et de la modélisation, dans la plupart de vos projets, il est question de classes « requêtes » ;

  j’attire votre attention sur le fait une requête peut être composée depuis le terminal, et que l’URL de base du site est « remplie » à postériori avec vos paramètres.

  Un niveau d’abstraction supplémentaire (sur le diag de classe par ex., avec de l'héritage ?) est peut être à rajouter, car les url de base des sites ne sont pas toutes les mêmes... => cela prendra sens lors des implémentations, prendre du recul vous permettra probablement de voir où je veut en venir.
